import { Routes, RouterModule }  from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { Approvals } from './approvals.component';

import { UsersRequest } from './components/usersRequest/usersRequest.component';
import { EventsRequest } from './components/eventsRequest/eventsRequest.component';

// noinspection TypeScriptValidateTypes
export const routes: Routes = [
  {
    path: '',
    component: Approvals,
    children: [
      { path: 'usersRequest', component: UsersRequest },
      { path: 'eventsRequest', component: EventsRequest }
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
