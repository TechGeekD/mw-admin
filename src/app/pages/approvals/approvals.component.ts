import {Component} from '@angular/core';

@Component({
  selector: 'approvals',
  template: '<router-outlet></router-outlet>',
})
export class Approvals {

  constructor() { }

}
