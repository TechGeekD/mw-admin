import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { NgaModule } from '../../theme/nga.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { routing }       from './approvals.routing';
import { DropdownModule } from 'ng2-bootstrap';

import { Approvals } from './approvals.component';

import { UsersRequest } from './components/usersRequest/usersRequest.component';
import { EventsRequest } from './components/eventsRequest/eventsRequest.component';

import { UserButtons } from './components/usersRequest/buttons/buttons.component';
import { EventButtons } from './components/eventsRequest/buttons/buttons.component';
import { DropdownUser } from './components/usersRequest/buttons/components/dropdownButtons';
import { DropdownEvent } from './components/eventsRequest/buttons/components/dropdownButtons';

import { UsersRequestService } from './components/usersRequest/usersRequest.service';
import { EventsRequestService } from './components/eventsRequest/eventsRequest.service';
import { DropdownEventsService } from './components/eventsRequest/buttons/components/dropdownButtons/dropdownButtons.service';
import { DropdownUsersService } from './components/usersRequest/buttons/components/dropdownButtons/dropdownButtons.service';

@NgModule({
  imports: [
    CommonModule,
    NgaModule,
    routing,
    DropdownModule.forRoot(),
    Ng2SmartTableModule
  ],
  declarations: [
    Approvals,
    UsersRequest,
    EventsRequest,
    UserButtons,
    EventButtons,
    DropdownUser,
    DropdownEvent
  ],
  providers: [
    UsersRequestService,
    EventsRequestService,
    DropdownUsersService,
    DropdownEventsService
  ]
})
export class ApprovalsModule {}
