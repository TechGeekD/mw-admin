import {Component, HostListener} from '@angular/core';

import { DropdownUsersService } from './dropdownButtons.service';

@Component({
  selector: 'dropdown-user',
  templateUrl: './dropdownButtons.html'
})

// TODO: appendToBody does not implemented yet, waiting for it
export class DropdownUser {

  eventData: any;
  eventList: any;
  selectedEvent: any = 'Select Event';
  selectedMemberType: any = 'Individual';

  constructor(protected service: DropdownUsersService) {
    this.service.getData().then((data) => {
      this.eventList = data;
    });
  }

  setMembersType( memberType ){
    this.service.callComponentMethod( null, memberType );
    this.selectedMemberType = memberType;
  }

  setEventMembers( evId, evName ){
    console.log(evId);
    this.service.callComponentMethod( evId, this.selectedMemberType );
    this.selectedEvent = evName;
  }

}
