import {Component} from '@angular/core';

import { UsersRequestService } from './usersRequest.service';
import { DropdownUsersService } from './buttons/components/dropdownButtons/dropdownButtons.service';
import { LocalDataSource } from 'ng2-smart-table';

import 'style-loader!./usersRequest.scss';

@Component({
  selector: 'users-request',
  templateUrl: './usersRequest.html',
})
export class UsersRequest {

  settings: any = {
    mode: 'external',
    // actions: {
    //   add: false
    // },
    add: {
      addButtonContent: '<i class="fa fa-search"></i>',
      createButtonContent: '<i class="ion-checkmark"></i>',
      confirmCreate: true,
      cancelButtonContent: '<i class="ion-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="ion-checkmark"></i>',
      saveButtonContent: '<i class="ion-checkmark"></i>',
      confirmSave: true,
      cancelButtonContent: '<i class="ion-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="ion-close"></i>',
      confirmDelete: true
    },
    columns: {
      event_name: {
        title: 'Event Name',
        type: 'string'
      },
      uname: {
        title: 'Username',
        type: 'string'
      },
      fullname: {
        title: 'Full Name',
        type: 'string'
      },
      phone: {
        title: 'Phone No',
        type: 'string'
      },
      email: {
        title: 'E-mail',
        type: 'string'
      },
      rfid: {
        title: 'RFID',
        type: 'number'
      }
    }
  };

  source: LocalDataSource = new LocalDataSource();

  tableType: any = 'Individual';

  constructor(protected service: UsersRequestService, protected communicationService: DropdownUsersService) {
    this.service.getData(null, null, this.tableType).then((data) => {
      this.source.load(data);
    });
    this.communicationService.componentMethodCalled$.subscribe(
      (data) => {
        console.log(data);
        if (data.evId != null ){
          this.getData(data.evId, this.tableType);
        } else {
          this.changeTableType( data.memberType );
        }
      }
    );
  }

  getData( evId, tableType ){
    this.service.getData('update', evId, tableType).then((data) => {
      this.source.load(data);
      this.source.refresh();
    });
  }

  changeTableType( type ){
    console.log(type);
    this.tableType = type;

    if ( type === 'Individual' ){
        this.settings = {
          mode: 'external',
          add: {
            addButtonContent: '<i class="fa fa-search"></i>',
            createButtonContent: '<i class="ion-checkmark"></i>',
            confirmCreate: true,
            cancelButtonContent: '<i class="ion-close"></i>',
          },
          edit: {
            editButtonContent: '<i class="ion-checkmark"></i></i>',
            saveButtonContent: '<i class="ion-checkmark"></i>',
            confirmSave: true,
            cancelButtonContent: '<i class="ion-close"></i>',
          },
          delete: {
            deleteButtonContent: '<i class="ion-close"></i>',
            confirmDelete: true
          },
          columns: {
            event_name: {
              title: 'Event Name',
              type: 'string'
            },
            uname: {
              title: 'Username',
              type: 'string'
            },
            fullname: {
              title: 'Full Name',
              type: 'string'
            },
            phone: {
              title: 'Phone No',
              type: 'string'
            },
            email: {
              title: 'E-mail',
              type: 'string'
            },
            rfid: {
              title: 'RFID',
              type: 'number'
            }
          }
        };
        this.getData( null, this.tableType );
    } else {
        this.settings = {
          mode: 'external',
          add: {
            addButtonContent: '<i class="fa fa-search"></i>',
            createButtonContent: '<i class="ion-checkmark"></i>',
            confirmCreate: true,
            cancelButtonContent: '<i class="ion-close"></i>',
          },
          edit: {
            editButtonContent: '<i class="ion-checkmark"></i>',
            confirmSave: true,
            cancelButtonContent: '<i class="ion-close"></i>',
          },
          delete: {
            deleteButtonContent: '<i class="ion-close"></i>',
            confirmDelete: true
          },
          columns: {
            event_name: {
              title: 'Event Name',
              type: 'string'
            },
            group_name: {
              title: 'Group Name',
              type: 'string'
            },
            member: {
              title: 'Leader',
              type: 'string'
            }
          }
        };
        this.getData( null, this.tableType );
    }
    this.source.refresh();
  }

  onApprove(event){
    if (window.confirm('Are you sure you want to Allow User to Join?')) {
      this.service.saveData(event.data);
      this.source.remove(event.data);
      this.source.refresh();
    } else {
      console.log('Cancled Clicked'); 
    }
    console.log(event);
  }

  onDelete(event){
    if (window.confirm('Are you sure you want to Reject User Request?')) {
      this.service.deleteData(event.data);
      this.source.remove(event.data);
      this.source.refresh();
    } else {
      console.log('Cancled Clicked'); 
    }
    console.log(event);
  }

  onSaveConfirm(event): void{
    if (window.confirm('Are you sure you want to Save?')) {
      event.confirm.resolve();
      this.service.saveData(event.newData);
    } else {
      event.confirm.reject();
    }
  }

  onCreateConfirm(event): void{
    if (window.confirm('Are you sure you want to Create?')) {
      event.confirm.resolve();
      this.service.setData(event.newData);
    } else {
      event.confirm.reject();
    }
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
      this.service.deleteData(event.data);
    } else {
      event.confirm.reject();
    }
  }
}
