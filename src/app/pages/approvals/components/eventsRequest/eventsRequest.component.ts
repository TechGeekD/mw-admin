import {Component} from '@angular/core';

import { EventsRequestService } from './eventsRequest.service';
import { DropdownEventsService } from './buttons/components/dropdownButtons/dropdownButtons.service';
import { LocalDataSource } from 'ng2-smart-table';

import 'style-loader!./eventsRequest.scss';

@Component({
  selector: 'events-request',
  templateUrl: './eventsRequest.html',
})
export class EventsRequest {

  settings: any = {
    mode: 'external',
    add: {
      addButtonContent: '<i class="fa fa-search"></i>',
      createButtonContent: '<i class="ion-checkmark"></i>',
      confirmCreate: true,
      cancelButtonContent: '<i class="ion-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="ion-checkmark"></i>',
      saveButtonContent: '<i class="ion-checkmark"></i>',
      confirmSave: true,
      cancelButtonContent: '<i class="ion-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="ion-close"></i>',
      confirmDelete: true
    },
    columns: {
      req_title: {
        title: 'Title',
        type: 'string'
      },
      req_desc: {
        title: 'Description',
        type: 'string'
      },
      fullname: {
        title: 'Requester',
        type: 'string'
      },
      category: {
        title: 'Category',
        type: 'string'
      },
      email: {
        title: 'E-mail',
        type: 'string'
      },
      phone: {
        title: 'Phone No',
        type: 'number'
      }
    }
  };

  source: LocalDataSource = new LocalDataSource();

  tableType: any = 'Individual';
  indi: any = [];
  group: any = [];

  constructor(protected service: EventsRequestService, protected communicationService: DropdownEventsService) {
    this.service.getData( null ).then((data) => {
      this.source.load(data);
    });
    this.communicationService.componentMethodCalled$.subscribe(
      (data) => {
        console.log(data);
        this.getData(data.reqType);
      }
    );
  }

  getData(reqType){
    this.indi = [];
    this.group = [];
    this.service.getData( reqType ).then((data) => {
      data.forEach(element => {
          if (element.dp == null){
            this.group.push(element);
          } else {
            this.indi.push(element);
          }
      });
      if ( this.tableType === 'Individual' ){
        this.source.load(this.indi);
      } else if ( this.tableType === 'Group' ) {
        this.source.load(this.group);
      }
      this.source.refresh();
    });
  }

  changeTableType( type ){
    console.log(type);
    this.tableType = type;

    if ( type === 'Individual' ){
        this.settings = {
          add: {
            addButtonContent: '<i class="ion-ios-plus-outline"></i>',
            createButtonContent: '<i class="ion-checkmark"></i>',
            confirmCreate: true,
            cancelButtonContent: '<i class="ion-close"></i>',
          },
          edit: {
            editButtonContent: '<i class="ion-edit"></i>',
            saveButtonContent: '<i class="ion-checkmark"></i>',
            confirmSave: true,
            cancelButtonContent: '<i class="ion-close"></i>',
          },
          delete: {
            deleteButtonContent: '<i class="ion-trash-a"></i>',
            confirmDelete: true
          },
          columns: {
            uname: {
              title: 'Username',
              type: 'string'
            },
            fullname: {
              title: 'Full Name',
              type: 'string'
            },
            phone: {
              title: 'Phone No',
              type: 'string'
            },
            email: {
              title: 'E-mail',
              type: 'string'
            },
            rfid: {
              title: 'RFID',
              type: 'number'
            }
          }
        };
        this.source.load(this.indi);
    } else {
        this.settings = {
          add: {
            addButtonContent: '<i class="ion-ios-plus-outline"></i>',
            createButtonContent: '<i class="ion-checkmark"></i>',
            confirmCreate: true,
            cancelButtonContent: '<i class="ion-close"></i>',
          },
          edit: {
            editButtonContent: '<i class="ion-edit"></i>',
            saveButtonContent: '<i class="ion-checkmark"></i>',
            confirmSave: true,
            cancelButtonContent: '<i class="ion-close"></i>',
          },
          delete: {
            deleteButtonContent: '<i class="ion-trash-a"></i>',
            confirmDelete: true
          },
          columns: {
            group_name: {
              title: 'Group Name',
              type: 'string'
            },
            member: {
              title: 'Leader',
              type: 'string'
            }
          }
        };
        this.source.load(this.group);
    }
    this.source.refresh();
  }
 
 onApprove(event){
    if (window.confirm('Are you sure you want to Add New Requested Event?')) {
      this.service.saveData(event.data);
      this.source.remove(event.data);
      this.source.refresh();
    } else {
      console.log('Cancled Clicked'); 
    }
    console.log(event);
  }

  onDelete(event){
    if (window.confirm('Are you sure you want to Reject User Request?')) {
      this.service.deleteData(event.data);
      this.source.remove(event.data);
      this.source.refresh();
    } else {
      console.log('Cancled Clicked'); 
    }
    console.log(event);
  }

  onSaveConfirm(event): void{
    if (window.confirm('Are you sure you want to Save?')) {
      event.confirm.resolve();
      this.service.saveData(event.newData);
    } else {
      event.confirm.reject();
    }
  }

  onCreateConfirm(event): void{
    if (window.confirm('Are you sure you want to Create?')) {
      event.confirm.resolve();
      this.service.setData(event.newData);
    } else {
      event.confirm.reject();
    }
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
      this.service.deleteData(event.data);
    } else {
      event.confirm.reject();
    }
  }
}
