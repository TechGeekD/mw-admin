import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';


@Injectable()
export class DropdownEventsService {


  // memberData: any;
  eventList: any;


  private componentMethodCallSource = new Subject<any>();

  componentMethodCalled$ = this.componentMethodCallSource.asObservable();

  private api = 'http://localhost:90/admin';

  private headers = new Headers({'Content-Type': 'application/json'});


  constructor (private http: Http ) {}

  callComponentMethod( reqType ) {
    this.componentMethodCallSource.next( {reqType: reqType} );
  }

  getData(): Promise<any> {
    this.getEvents();
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(this.eventList);
      }, 2000);
    });
  }

  getEvents() {
    let url = this.api + '/events';
    this.http.get(url, this.headers)
        .map((res: Response) => res.json()).catch(this.handleError).subscribe(data => {
          if (data) {
            this.eventList = data;
          } else {
            console.log('NO EventDATA FETCHED !!');
          }
        });
  }

  private handleError (error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}

export class RequestOptions {
  constructor(public body?: any) { }
}
