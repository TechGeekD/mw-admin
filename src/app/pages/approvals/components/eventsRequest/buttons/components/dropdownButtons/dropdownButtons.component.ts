import {Component, HostListener} from '@angular/core';

import { DropdownEventsService } from './dropdownButtons.service';

@Component({
  selector: 'dropdown-event',
  templateUrl: './dropdownButtons.html'
})

// TODO: appendToBody does not implemented yet, waiting for it
export class DropdownEvent {

  x: any = ['hello', 'World'];

  eventData: any;
  eventList: any;
  selectedEvent: any = 'Select Event Category';
  selectedMemberType: any = 'Individual';

  constructor(protected service: DropdownEventsService) {
    this.service.getData().then((data) => {
      this.eventList = data;
    });
   }

  // setMembersType( memberType ){
  //   this.service.callComponentMethod( null, memberType );
  //   this.selectedMemberType = memberType;
  // }

  setEventRequests( reqType ){
    console.log(reqType);
    this.service.callComponentMethod( reqType );
    if (reqType != null ){
      this.selectedEvent = reqType;
    } else {
      this.selectedEvent = 'Select Event Category';
    }
  }

}
