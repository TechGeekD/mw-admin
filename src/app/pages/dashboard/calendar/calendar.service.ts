import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { BaThemeConfigProvider } from '../../../theme';

@Injectable()
export class CalendarService {

  date: any;
  today: any;
  eventData: any = false;

  private api = 'http://localhost:90/admin';

  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http: Http, private _baConfig: BaThemeConfigProvider) {
    this.getEvents();
    this.getDate();
  }

  getData() {

    let dashboardColors = this._baConfig.get().colors.dashboard;
    // return new Promise((resolve, reject) => {
      // let time = setInterval(() =>{
        // if ( this.eventData ){
          // clearInterval(time);
          // resolve(
            return {
            header: {
              left: 'prev,next today',
              center: 'title',
              right: 'month,agendaWeek,agendaDay'
            },
            defaultDate: this.today,
            selectable: true,
            selectHelper: true,
            editable: true,
            eventLimit: true,
            events: this.eventData
          }
          // );
        // }
      // });
    // });
  }

  getDate(){
    this.date = new Date();
    this.today = this.date.getFullYear() + '-' + (this.date.getMonth() + 1) + '-' + this.date.getDate();
    console.log(this.today);
  }

  getEvents() {
    let url = this.api + '/events';
    this.eventData = [];
    let dashboardColors = this._baConfig.get().colors.dashboard;
    this.http.get(url, this.headers)
        .map((res: Response) => res.json()).catch(this.handleError).subscribe(data => {
          if (data) {
            let i = 0;
            data.forEach(element => {
              let date = new Date(element.event_date);
              let day = date.getDate();
              let monthIndex = date.getMonth() + 1;
              let year = date.getFullYear();
              let eventDate;
              if ( monthIndex < 10 && day < 10 ){
                eventDate = year + '-' + '0' + monthIndex + '-' + '0' + day;
              } else if ( day < 10 ){
                eventDate = year + '-' + monthIndex + '-' + '0' + day;
              } else if ( monthIndex < 10  ){
                eventDate = year + '-' + '0' + monthIndex + '-' + day;
              }
              let eventName = element.event_name;
              this.eventData.push({ title: eventName, start: eventDate, color: dashboardColors.silverTree });
            });
            console.log(this.eventData);
          } else {
            console.log('NO EventDATA FETCHED !!');
          }
        });
  }

  private handleError (error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
