import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {BaThemeConfigProvider, colorHelper} from '../../../theme';

@Injectable()
export class PieChartService {

  totalUsers: any = 14232;
  totalEvents: any = 2534;
  totalActiveUsers: any = 1123;
  totalCompletedEvents: any = 1356;

  count: any;

  private api = 'http://localhost:90/admin';

  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http: Http, private _baConfig:BaThemeConfigProvider) {
  }

  getStats(){
    let url = this.api + '/stats/get';
    this.http.get(url, this.headers)
        .map((res: Response) => res.json()).catch(this.handleError).subscribe(data => {
          this.count = 0;
          data.forEach(element => {
            console.log(element);
            if( element[0].totalUsers ){
              this.totalUsers = element[0].totalUsers;
              this.count += 1;
              console.log(this.count);
            } else if ( element[0].totalEvents ){
              this.totalEvents = element[0].totalEvents;
              this.count += 1;
              console.log(this.count);
            } else if ( element[0].activeUsers ) {
              this.totalActiveUsers = element[0].activeUsers;
              this.count += 1;
              console.log(this.count);
            } else if ( element[0].totalCompletedEvents ) {
              this.totalCompletedEvents = element[0].totalCompletedEvents;
              this.count += 1;
              console.log(this.count);
            }
          });
        });
  }

  getData() {
    this.getStats();
    let pieColor = this._baConfig.get().colors.custom.dashboardPieChart;
    let chartData;
    // let time = setInterval(() =>{
    //   if( this.count === 4 ){
    //     clearInterval(time);
        chartData = [
          {
            color: pieColor,
            description: 'Total Members',
            stats: this.totalUsers,
            icon: 'person',
          }, {
            color: pieColor,
            description: 'Total Events',
            stats: this.totalEvents,
            icon: 'money',
          }, {
            color: pieColor,
            description: 'Active Users',
            stats: this.totalActiveUsers,
            icon: 'face',
          }, {
            color: pieColor,
            description: 'Event Compeleted',
            stats: this.totalCompletedEvents,
            icon: 'refresh',
          }
        ];
      // }
    // }, 1000);
    return chartData;
  }

  private extractData(res: Response) {
    let body = res.json();
    return body.data || { };
  }

  private handleError (error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
