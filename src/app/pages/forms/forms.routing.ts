import { Routes, RouterModule }  from '@angular/router';

import { Forms } from './forms.component';
import { Events } from './components/events/events.component';
import { Members } from './components/members/members.component';
import { Gallery } from './components/gallery/gallery.component';
import { EvMembers } from './components/event-members/event-members.component';

import { Inputs } from './components/inputs/inputs.component';
import { Layouts } from './components/layouts/layouts.component';

// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: Forms,
    children: [
      { path: 'events', component: Events },
      { path: 'members', component: Members },
      { path: 'gallery', component: Gallery },
      { path: 'eventMembers', component: EvMembers },
      { path: 'inputs', component: Inputs },
      { path: 'layouts', component: Layouts }
    ]
  }
];

export const routing = RouterModule.forChild(routes);
