import { Component } from '@angular/core';

import { EventEntryService } from './eventEntry.service';
import { LocalDataSource } from 'ng2-smart-table';

import 'style-loader!./eventEntry.scss';

@Component({
  selector: 'event-entry',
  templateUrl: './eventEntry.html',
})
export class EventEntry {

  query: string = '';

  settings = {
    add: {
      addButtonContent: '<i class="ion-ios-plus-outline"></i>',
      createButtonContent: '<i class="ion-checkmark"></i>',
      confirmCreate: true,
      cancelButtonContent: '<i class="ion-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="ion-edit"></i>',
      saveButtonContent: '<i class="ion-checkmark"></i>',
      confirmSave: true,
      cancelButtonContent: '<i class="ion-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="ion-trash-a"></i>',
      confirmDelete: true
    },
    columns: {
      event_name: {
        title: 'Name',
        type: 'string'
      },
      event_desc: {
        title: 'Description',
        type: 'string'
      },
      category: {
        title: 'Category',
        type: 'string'
      },
      event_city: {
        title: 'City',
        type: 'string'
      },
      event_date: {
        title: 'Date',
        type: 'string'
      },
      interest: {
        title: 'Interest',
        type: 'string'
      },
      repo_points: {
        title: 'Repo Points',
        type: 'number'
      }
    }
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(protected service: EventEntryService) {
    this.service.getData().then((data) => {
      console.log(data);
      this.source.load(data);
    });
  }

  onSaveConfirm(event): void{
    if (window.confirm('Are you sure you want to Save?')) {
      event.confirm.resolve();
      this.service.saveData(event.newData);
    } else {
      event.confirm.reject();
    }
  }

  onCreateConfirm(event): void{
    if (window.confirm('Are you sure you want to Create?')) {
      event.confirm.resolve();
      console.log(event.newData);
      this.service.setData(event.newData);
    } else {
      event.confirm.reject();
    }
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
      this.service.deleteData(event.data);
    } else {
      event.confirm.reject();
    }
  }
}
