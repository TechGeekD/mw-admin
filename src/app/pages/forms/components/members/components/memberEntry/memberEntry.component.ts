import { Component } from '@angular/core';

import { MemberEntryService } from './memberEntry.service';
import { LocalDataSource } from 'ng2-smart-table';

import 'style-loader!./memberEntry.scss';

@Component({
  selector: 'member-entry',
  templateUrl: './memberEntry.html',
})
export class MemberEntry {

  query: string = '';

  settings = {
    add: {
      addButtonContent: '<i class="ion-ios-plus-outline"></i>',
      createButtonContent: '<i class="ion-checkmark"></i>',
      confirmCreate: true,
      cancelButtonContent: '<i class="ion-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="ion-edit"></i>',
      saveButtonContent: '<i class="ion-checkmark"></i>',
      confirmSave: true,
      cancelButtonContent: '<i class="ion-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="ion-trash-a"></i>',
      confirmDelete: true
    },
    columns: {
      uname: {
        title: 'Username',
        type: 'number'
      },
      fullname: {
        title: 'Full Name',
        type: 'string'
      },
      phone: {
        title: 'Phone No',
        type: 'string'
      },
      email: {
        title: 'E-mail',
        type: 'string'
      },
      rfid: {
        title: 'RFID',
        type: 'number'
      }
    }
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(protected service: MemberEntryService) {
    this.service.getData().then((data) => {
      this.source.load(data);
    });
  }

  onSaveConfirm(event): void{
    if (window.confirm('Are you sure you want to Save?')) {
      event.confirm.resolve();
      this.service.saveData(event.newData);
    } else {
      event.confirm.reject();
    }
  }

  onCreateConfirm(event): void{
    if (window.confirm('Are you sure you want to Create?')) {
      event.confirm.resolve();
      this.service.setData(event.newData);
    } else {
      event.confirm.reject();
    }
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
      this.service.deleteData(event.data);
    } else {
      event.confirm.reject();
    }
  }
}
