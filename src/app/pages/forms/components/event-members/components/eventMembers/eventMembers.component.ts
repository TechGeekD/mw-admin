import {Component} from '@angular/core';

import { EventMemberEntryService } from './eventMembers.service';
import { DropdownButtonsService } from './buttons/components/dropdownButtons/dropdownButtons.service';
import { LocalDataSource } from 'ng2-smart-table';

import 'style-loader!./eventMembers.scss';

@Component({
  selector: 'event-members',
  templateUrl: './eventMembers.html',
})
export class EventMembers {

  settings: any = {
    add: {
      addButtonContent: '<i class="ion-ios-plus-outline"></i>',
      createButtonContent: '<i class="ion-checkmark"></i>',
      confirmCreate: true,
      cancelButtonContent: '<i class="ion-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="ion-edit"></i>',
      saveButtonContent: '<i class="ion-checkmark"></i>',
      confirmSave: true,
      cancelButtonContent: '<i class="ion-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="ion-trash-a"></i>',
      confirmDelete: true
    },
    columns: {
      uname: {
        title: 'Username',
        type: 'string'
      },
      fullname: {
        title: 'Full Name',
        type: 'string'
      },
      phone: {
        title: 'Phone No',
        type: 'string'
      },
      email: {
        title: 'E-mail',
        type: 'string'
      },
      rfid: {
        title: 'RFID',
        type: 'number'
      },
      ev_leader: {
        title: 'Event Leader',
        type: 'stirng'
      }
    }
  };

  source: LocalDataSource = new LocalDataSource();

  tableType: any = 'Individual';
  indi: any = [];
  group: any = [];

  constructor(protected service: EventMemberEntryService, protected communicationService: DropdownButtonsService) {
    this.service.getData(null, null).then((data) => {
      this.source.load(data);
    });
    this.communicationService.componentMethodCalled$.subscribe(
      (data) => {
        console.log(data);
        if (data.evId != null ){
          this.getData(data.evId);
        } else {
          this.changeTableType( data.memberType );
        }
      }
    );
  }

  getData(evId){
    this.indi = [];
    this.group = [];
    this.service.getData('update', evId).then((data) => {
      data.forEach(element => {
          if (element.dp == null){
            this.group.push(element);
          } else {
            this.indi.push(element);
          }
      });
      if ( this.tableType === 'Individual' ){
        this.source.load(this.indi);
      } else if ( this.tableType === 'Group' ) {
        this.source.load(this.group);
      }
      this.source.refresh();
    });
  }

  changeTableType( type ){
    console.log(type);
    this.tableType = type;

    if ( type === 'Individual' ){
        this.settings = {
          add: {
            addButtonContent: '<i class="ion-ios-plus-outline"></i>',
            createButtonContent: '<i class="ion-checkmark"></i>',
            confirmCreate: true,
            cancelButtonContent: '<i class="ion-close"></i>',
          },
          edit: {
            editButtonContent: '<i class="ion-edit"></i>',
            saveButtonContent: '<i class="ion-checkmark"></i>',
            confirmSave: true,
            cancelButtonContent: '<i class="ion-close"></i>',
          },
          delete: {
            deleteButtonContent: '<i class="ion-trash-a"></i>',
            confirmDelete: true
          },
          columns: {
            uname: {
              title: 'Username',
              type: 'string'
            },
            fullname: {
              title: 'Full Name',
              type: 'string'
            },
            phone: {
              title: 'Phone No',
              type: 'string'
            },
            email: {
              title: 'E-mail',
              type: 'string'
            },
            rfid: {
              title: 'RFID',
              type: 'number'
            },
            ev_leader: {
              title: 'Event Leader',
              type: 'stirng'
            }
          }
        };
        this.source.load(this.indi);
    } else {
        this.settings = {
          add: {
            addButtonContent: '<i class="ion-ios-plus-outline"></i>',
            createButtonContent: '<i class="ion-checkmark"></i>',
            confirmCreate: true,
            cancelButtonContent: '<i class="ion-close"></i>',
          },
          edit: {
            editButtonContent: '<i class="ion-edit"></i>',
            saveButtonContent: '<i class="ion-checkmark"></i>',
            confirmSave: true,
            cancelButtonContent: '<i class="ion-close"></i>',
          },
          delete: {
            deleteButtonContent: '<i class="ion-trash-a"></i>',
            confirmDelete: true
          },
          columns: {
            group_name: {
              title: 'Group Name',
              type: 'string'
            },
            member: {
              title: 'Leader',
              type: 'string'
            }
          }
        };
        this.source.load(this.group);
    }
    this.source.refresh();
  }

  onSaveConfirm(event): void{
    if (window.confirm('Are you sure you want to Save?')) {
      event.confirm.resolve();
      this.service.saveData(event.newData);
    } else {
      event.confirm.reject();
    }
  }

  onCreateConfirm(event): void{
    if (window.confirm('Are you sure you want to Create?')) {
      event.confirm.resolve();
      this.service.setData(event.newData);
    } else {
      event.confirm.reject();
    }
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
      this.service.deleteData(event.data);
    } else {
      event.confirm.reject();
    }
  }
}
