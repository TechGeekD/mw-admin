import {Component, HostListener} from '@angular/core';

import { DropdownButtonsService } from './dropdownButtons.service';

@Component({
  selector: 'dropdown-buttons',
  templateUrl: './dropdownButtons.html'
})

// TODO: appendToBody does not implemented yet, waiting for it
export class DropdownButtons {

  x: any = ['hello', 'World'];

  eventData: any;
  eventList: any;
  selectedEvent: any = 'Select Event';
  selectedMemberType: any = 'Individual';

  constructor(protected service: DropdownButtonsService) {
    this.service.getData().then((data) => {
      this.eventList = data;
    });
   }

  setMembersType( memberType ){
    this.service.callComponentMethod( null, memberType );
    this.selectedMemberType = memberType;
  }

  setEventMembers( evId, evName ){
    console.log(evId);
    this.service.callComponentMethod( evId, 'Individual' );
    this.selectedEvent = evName;
  }

}
