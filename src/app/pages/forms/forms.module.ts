import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule as AngularFormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { routing }       from './forms.routing';
import { DropdownModule } from 'ng2-bootstrap';

import { RatingModule } from 'ng2-bootstrap';
import { Forms } from './forms.component';
import { Inputs } from './components/inputs';
import { Layouts } from './components/layouts';


import { Events } from './components/events';
import { Members } from './components/members';
import { Gallery } from './components/gallery';
import { EvMembers } from './components/event-members/event-members.component';

import { Buttons } from './components/event-members/components/eventMembers/buttons/buttons.component';
import { DropdownButtons } from './components/event-members/components/eventMembers/buttons/components/dropdownButtons';

import { EventEntry } from './components/events/components/eventEntry';
import { MemberEntry } from './components/members/components/memberEntry';
import { GalleryEntry } from './components/gallery/components/galleryEntry';
import { EventMembers } from './components/event-members/components/eventMembers';

import { EventEntryService } from './components/events/components/eventEntry/eventEntry.service';
import { MemberEntryService } from './components/members/components/memberEntry/memberEntry.service';
import { EventMemberEntryService } from './components/event-members/components/eventMembers/eventMembers.service';
import { DropdownButtonsService } from './components/event-members/components/eventMembers/buttons/components/dropdownButtons/dropdownButtons.service';

import { StandardInputs } from './components/inputs/components/standardInputs';
import { ValidationInputs } from './components/inputs/components/validationInputs';
import { GroupInputs } from './components/inputs/components/groupInputs';
import { CheckboxInputs } from './components/inputs/components/checkboxInputs';
import { Rating } from './components/inputs/components/ratinginputs';
import { SelectInputs } from './components/inputs/components/selectInputs';

import { InlineForm } from './components/layouts/components/inlineForm';
import { BlockForm } from './components/layouts/components/blockForm';
import { HorizontalForm } from './components/layouts/components/horizontalForm';
import { BasicForm } from './components/layouts/components/basicForm';
import { WithoutLabelsForm } from './components/layouts/components/withoutLabelsForm';

@NgModule({
  imports: [
    CommonModule,
    AngularFormsModule,
    NgaModule,
    RatingModule.forRoot(),
    DropdownModule.forRoot(),
    routing,
    Ng2SmartTableModule
  ],
  declarations: [
    Layouts,
    Events, //
    Members,
    Gallery,
    EvMembers,
    Buttons,
    DropdownButtons,
    Inputs,
    Forms,
    StandardInputs,
    EventEntry, //
    MemberEntry,
    GalleryEntry,
    EventMembers,
    ValidationInputs,
    GroupInputs,
    CheckboxInputs,
    Rating,
    SelectInputs,
    InlineForm,
    BlockForm,
    HorizontalForm,
    BasicForm,
    WithoutLabelsForm
  ],
  providers: [
    EventEntryService,
    MemberEntryService,
    EventMemberEntryService,
    DropdownButtonsService
  ]
})
export class FormsModule {
}
